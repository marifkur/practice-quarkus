package org.acme.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class TagRequest {

    private String id;

    @NotEmpty
    @NotNull
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
