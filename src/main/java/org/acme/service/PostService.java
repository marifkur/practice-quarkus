package org.acme.service;

import org.acme.entity.Post;
import org.acme.entity.Tag;
import org.acme.exceptions.DataNotFoundException;
import org.acme.repository.PostRepository;
import org.acme.repository.TagRepository;
import org.acme.request.PostRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    PostRepository postRepository;

    @Autowired
    TagRepository tagRepository;

    @Transactional
    public Post insert(PostRequest postRequest) {
        Post postPre = new Post();
        List<Tag> tagList = new ArrayList<>();

        if (!StringUtils.isEmpty(postRequest.getId())) {
            Optional<Post> postOptional = postRepository.findById(postRequest.getId());
            if (postOptional.isPresent()) {
                postPre = postOptional.get();
            }else
                throw new DataNotFoundException("Post Not Found with id : "+postRequest.getId());
        }

        if (postRequest.getTagIdList() != null && !postRequest.getTagIdList().isEmpty()) {
            for (String id : postRequest.getTagIdList()) {
                Optional<Tag> tagOptional = tagRepository.findById(id);
                if (!tagOptional.isPresent())
                    throw new DataNotFoundException("Data Tag Not Found with id : " + id);

                if (!StringUtils.isEmpty(postPre.getId()) && postPre.getTags().contains(tagOptional))
                    continue;
                tagList.add(tagOptional.get());
            }
        }

        postPre.getTags().addAll(tagList);
        postPre.setContent(postRequest.getContent());
        postPre.setTitle(postRequest.getTitle());

        return postRepository.save(postPre);
    }

    public Optional<Post> findById(String id) {
        return postRepository.findById(id);
    }

    public void delete(Post post) {
        postRepository.deleteById(post.getId());
    }

    public List findAll() {
        return postRepository.findAll();
    }
}
