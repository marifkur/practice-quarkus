package org.acme.service;

import org.acme.entity.Tag;
import org.acme.exceptions.DataNotFoundException;
import org.acme.repository.TagRepository;
import org.acme.request.TagRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    public Tag insert(TagRequest tagRequest) {
        Tag tagPre = new Tag();
        if (!StringUtils.isEmpty(tagRequest.getId())) {
            Optional<Tag> tagOptional = tagRepository.findById(tagRequest.getId());
            if (tagOptional.isPresent()) {
                tagPre = tagOptional.get();
            }else
                throw new DataNotFoundException("Tag Not Found with id : "+tagRequest.getId());
        }

        tagPre.setLabel(tagRequest.getLabel());

        return tagRepository.save(tagPre);
    }

    public Optional<Tag> findById(String id) {
        return tagRepository.findById(id);
    }

    public void delete(Tag tag) {
        tagRepository.deleteById(tag.getId());
    }

    public List findAll() {
        return tagRepository.findAll();
    }
}
