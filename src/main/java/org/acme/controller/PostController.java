package org.acme.controller;

import org.acme.entity.Post;
import org.acme.exceptions.DataNotFoundException;
import org.acme.request.PostRequest;
import org.acme.response.ErrorRes;
import org.acme.response.JsonResponse;
import org.acme.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/post")
public class PostController {

    @Autowired
    PostService postService;

    @GET()
    public ResponseEntity<?> findAll() {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        List<Post> postList = postService.findAll();
        if (postList == null || postList.isEmpty()) {
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Post Not Found");
            error.setDetail("Post Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        data.put("postList", postList);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GET
    @Path("/detail")
    public ResponseEntity<?> detail(@QueryParam("id") String id) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        params.put("id", id);
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        Post post = postService.findById(id).orElse(null);
        if (post == null) {
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Post Not Found");
            error.setDetail("Post Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        data.put("post", post);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<?> create(@RequestBody PostRequest postRequest) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        try {
            Post postSave = postService.insert(postRequest);
            data.put("post", postSave);
            response.setData(data);

        } catch (DataNotFoundException e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<?> update(@RequestBody PostRequest postRequest) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        if (StringUtils.isEmpty(postRequest.getId())) {
            ErrorRes error = new ErrorRes();
            error.setCode("400");
            error.setStatus(HttpStatus.BAD_REQUEST.toString());
            error.setTitle("Please input id");
            error.setDetail("Please input id");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            Post postSave = postService.insert(postRequest);
            data.put("post", postSave);
            response.setData(data);
        } catch (DataNotFoundException e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DELETE
    public ResponseEntity<?> delete(@QueryParam("id") String id) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        params.put("id", id);
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        Post postCheck = postService.findById(id).orElse(null);
        if (postCheck == null){
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Post Not Found");
            error.setDetail("Post Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }

        postService.delete(postCheck);
        data.put("postDeleted", postCheck);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

}
