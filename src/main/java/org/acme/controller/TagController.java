package org.acme.controller;

import org.acme.entity.Tag;
import org.acme.exceptions.DataNotFoundException;
import org.acme.request.TagRequest;
import org.acme.response.ErrorRes;
import org.acme.response.JsonResponse;
import org.acme.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/tag")
public class TagController {

    @Autowired
    TagService tagService;

    @GET()
    public ResponseEntity<?> findAll() {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        List<Tag> tagList = tagService.findAll();
        if (tagList == null || tagList.isEmpty()) {
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Tag Not Found");
            error.setDetail("Tag Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        data.put("tagList", tagList);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @GET
    @Path("/detail")
    public ResponseEntity<?> detail(@QueryParam("id") String id) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        params.put("id", id);
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        Tag tag = tagService.findById(id).orElse(null);
        if (tag == null) {
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Tag Not Found");
            error.setDetail("Tag Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }
        data.put("tag", tag);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<?> create(@RequestBody TagRequest tagRequest) {

        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        try {
            Tag tagSave = tagService.insert(tagRequest);
            data.put("tag", tagSave);
            response.setData(data);

        } catch (DataNotFoundException e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<?> update(@RequestBody TagRequest tagRequest) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        if (StringUtils.isEmpty(tagRequest.getId())) {
            ErrorRes error = new ErrorRes();
            error.setCode("400");
            error.setStatus(HttpStatus.BAD_REQUEST.toString());
            error.setTitle("Please input id");
            error.setDetail("Please input id");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.BAD_REQUEST);
        }

        try {
            Tag tagSave = tagService.insert(tagRequest);
            data.put("tag", tagSave);
            response.setData(data);
        } catch (DataNotFoundException e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            ErrorRes error = new ErrorRes();
            error.setCode("500");
            error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.toString());
            error.setTitle(e.getMessage());
            error.setDetail(e.getMessage());
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(response, HttpStatus.OK);
    }

    @DELETE
    public ResponseEntity<?> delete(@QueryParam("id") String id) {
        JsonResponse<Map<String, Object>> response = new JsonResponse();
        Map<String, Object> data = new HashMap();
        Map<String, Object> params = new HashMap();
        Map<String, Object> dictionaries = new HashMap();

        params.put("id", id);
        dictionaries.put("params", params);
        response.setDictionaries(dictionaries);

        Tag tagCheck = tagService.findById(id).orElse(null);
        if (tagCheck == null){
            ErrorRes error = new ErrorRes();
            error.setCode("404");
            error.setStatus(HttpStatus.NOT_FOUND.toString());
            error.setTitle("Tag Not Found");
            error.setDetail("Tag Not found");
            response.getErrors().add(error);

            return new ResponseEntity(response, HttpStatus.NOT_FOUND);
        }

        tagService.delete(tagCheck);
        data.put("tagDeleted", tagCheck);
        response.setData(data);

        return new ResponseEntity(response, HttpStatus.OK);
    }

}
