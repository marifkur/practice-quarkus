package org.acme.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonResponse<T> {

    List<ErrorRes> errors = new ArrayList();

    T data;

    Map<String,?> dictionaries = new HashMap();

    public List<ErrorRes> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorRes> errors) {
        this.errors = errors;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Map<String, ?> getDictionaries() {
        return dictionaries;
    }

    public void setDictionaries(Map<String, ?> dictionaries) {
        this.dictionaries = dictionaries;
    }
}
